#include "main.h"
#include "uart_ring_buffer.h"

#define UartRxQueueItems 13U
#define UartTxQueueItems 16U

#if 0
#define UartRxQueueItems 13U
#define UartTxQueueItems 17U
#endif

#define UartRxQueueItemsLen 17U
#define UartTxQueueItemsLen 13U


void RE_UART4_Init(void);
void RE_RPI_ReadMsg(uint8_t *p_Ec20RxMsg);
void RE_RPI_WriteMsg(uint8_t *p_Ec20TxMsg);