
#ifndef _UART_RING__BUFFER_H
#define _UART_RING__BUFFER_H

#include "stdlib.h"
#include "main.h"

/* change the size of the buffer here */
#define UART4_BUFFER_SIZE 150

typedef struct
{
  unsigned char buffer_uart4[UART4_BUFFER_SIZE];
  volatile unsigned int head_uart4;
  volatile unsigned int tail_uart4;
}ring_buffer_uart4;

/* Initialize the ring buffer */
void Ringbuf_Init(void);

/* Free buffer memory*/
extern void Ringbuf_UART4_Free(void);

/* once you hit 'enter' (\r\n), it copies the entire string to the buffer*/
extern void Get_UART4_String(char *buffer);

/*After the predefined string is reached, it will copy the numberofchars after that into the buffertosave
 *USAGE---->>> if (Get_after("some string", 8, buffer)) { do something here}
 * */
//uint8_t Get_After(char *string, uint8_t numberofchars, char *buffertosave);

/* the ISR for the uart. put it in the IRQ handler */
extern void UART_ISR(UART_HandleTypeDef *huart);

//store uart string 
extern void Store_UART4_Char(unsigned char c, ring_buffer_uart4 *buffer);

#endif
/*********************** (C) COPYRIGHT RACEnergy **********END OF FILE********/