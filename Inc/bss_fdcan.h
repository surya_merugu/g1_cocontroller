#ifndef __BSS_FDCAN_H
#define __BSS_FDCAN_H

#include "main.h"

#define TX_NODE_CoController         (uint16_t)0x00F0

#define RxQueueItems     16U
#define TxQueueItems     16U
#define RxQueueItemLen   13U   /*4 Byte id + 1 byte DLC + 8 byte payload*/
#define TxQueueItemLen   13U   /*4 Byte id + 1 byte DLC + 8 byte payload*/

#if 0
#define RxQueueItems     13U
#define TxQueueItems     14U
#define RxQueueItemLen   13U   /*4 Byte id + 1 byte DLC + 8 byte payload*/
#define TxQueueItemLen   13U   /*4 Byte id + 1 byte DLC + 8 byte payload*/
#endif


void RE_CAN_Format_TxMsg(uint8_t dest_node_id, uint8_t msg_id, uint8_t *p_data);
void RE_FDCAN2_Write_Msg(uint8_t *p_CanTxMsg);
void RE_FDCAN2_Read_Msg(uint8_t *p_CanRxMsg);
void RE_FDCAN1_Init(void);
void RE_FDCAN2_Init(void);

#endif