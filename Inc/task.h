#ifndef __TASK_H
#define __TASK_H
#include "main.h"

#define EVT_QUEUE_LEN 10
#define EVT_QUEUE_ITEM_LEN 2
#define TASK_QUEUE_LEN 10
#define TASK_QUEUE_ITEM_LEN 5

bool RE_Task_Pending(void);
void RE_load_system_status(void);
void RE_Create_EVT_Queue(void);
void RE_Create_TASK_Queue(void);


#endif