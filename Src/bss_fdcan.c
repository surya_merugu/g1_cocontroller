/**
  ******************************************************************************
  * @file           : .c
  * @brief          : CAN peripheral
  ******************************************************************************
  * @attention
  *
  ******************************************************************************
  */
#include "bss_fdcan.h"

FDCAN_HandleTypeDef hfdcan1, hfdcan2;
FDCAN_RxHeaderTypeDef RxHeader;
FDCAN_TxHeaderTypeDef TxHeader;
    
Queue_t *p_FdCanRxQueue, *p_FdCanTxQueue;
uint8_t packetCnt = 0;
static void FDCAN_Filter_Config(void);
static void RE_FDCAN_Start_Interrupt(void);

/**
  * @brief FDCAN2 Initialization Function
  * @param None
  * @retval None
  */
void RE_FDCAN1_Init(void)
{
  hfdcan1.Instance = FDCAN1;
  hfdcan1.Init.ClockDivider = FDCAN_CLOCK_DIV1;
  hfdcan1.Init.FrameFormat = FDCAN_FRAME_CLASSIC;
  hfdcan1.Init.Mode = FDCAN_MODE_NORMAL;
  hfdcan1.Init.AutoRetransmission = DISABLE;
  hfdcan1.Init.TransmitPause = DISABLE;
  hfdcan1.Init.ProtocolException = DISABLE;
  hfdcan1.Init.NominalPrescaler = 20;
  hfdcan1.Init.NominalSyncJumpWidth = 1;
  hfdcan1.Init.NominalTimeSeg1 = 14;
  hfdcan1.Init.NominalTimeSeg2 = 2;
  hfdcan1.Init.DataPrescaler = 20;
  hfdcan1.Init.DataSyncJumpWidth = 1;
  hfdcan1.Init.DataTimeSeg1 = 14;
  hfdcan1.Init.DataTimeSeg2 = 2;
  hfdcan1.Init.StdFiltersNbr = 0;
  hfdcan1.Init.ExtFiltersNbr = 1;
  hfdcan1.Init.TxFifoQueueMode = FDCAN_TX_FIFO_OPERATION;
  if (HAL_FDCAN_Init(&hfdcan1) != HAL_OK)
  {
    Error_Handler();
  }
  
    p_FdCanRxQueue = RE_CreateQueue(RxQueueItems, RxQueueItemLen);
    p_FdCanTxQueue = RE_CreateQueue(TxQueueItems, TxQueueItemLen);
    
    FDCAN_Filter_Config();
    RE_FDCAN_Start_Interrupt();
}

#if 0
/**
  * @brief FDCAN2 Initialization Function
  * @param None
  * @retval None
  */
void RE_FDCAN2_Init(void)
{

  /* USER CODE BEGIN FDCAN2_Init 0 */

  /* USER CODE END FDCAN2_Init 0 */

  /* USER CODE BEGIN FDCAN2_Init 1 */

  /* USER CODE END FDCAN2_Init 1 */
  hfdcan2.Instance = FDCAN2;
  hfdcan2.Init.FrameFormat = FDCAN_FRAME_CLASSIC;
  hfdcan2.Init.Mode = FDCAN_MODE_NORMAL;
  hfdcan2.Init.AutoRetransmission = DISABLE;
  hfdcan2.Init.TransmitPause = DISABLE;
  hfdcan2.Init.ProtocolException = DISABLE;
  hfdcan2.Init.NominalPrescaler = 1;
  hfdcan2.Init.NominalSyncJumpWidth = 1;
  hfdcan2.Init.NominalTimeSeg1 = 2;
  hfdcan2.Init.NominalTimeSeg2 = 2;
  hfdcan2.Init.DataPrescaler = 1;
  hfdcan2.Init.DataSyncJumpWidth = 1;
  hfdcan2.Init.DataTimeSeg1 = 1;
  hfdcan2.Init.DataTimeSeg2 = 1;
  hfdcan2.Init.StdFiltersNbr = 0;
  hfdcan2.Init.ExtFiltersNbr = 0;
  hfdcan2.Init.TxFifoQueueMode = FDCAN_TX_FIFO_OPERATION;
  if (HAL_FDCAN_Init(&hfdcan2) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN FDCAN2_Init 2 */

  /* USER CODE END FDCAN2_Init 2 */

}
#endif


static void FDCAN_Filter_Config(void)
{
    /*TODO: Add Filter to accept only CoController messages on the bus*/

    FDCAN_FilterTypeDef FDCAN_Filter;
    FDCAN_Filter.IdType       = FDCAN_EXTENDED_ID;
    FDCAN_Filter.FilterIndex  = 1;
    FDCAN_Filter.FilterType   = FDCAN_FILTER_RANGE_NO_EIDM;   
    FDCAN_Filter.FilterConfig = FDCAN_FILTER_TO_RXFIFO0;
    FDCAN_Filter.FilterID1    = 0x00000000;
    FDCAN_Filter.FilterID2    = 0x1FFFFFFF;
    if(HAL_FDCAN_ConfigFilter(&hfdcan1, &FDCAN_Filter) != HAL_OK)
    {
        Error_Handler();
    }
}


static void RE_FDCAN_Start_Interrupt(void)
{
    if(HAL_FDCAN_ActivateNotification(&hfdcan1, FDCAN_IT_TX_FIFO_EMPTY|FDCAN_IT_RX_FIFO0_FULL|\
                                        FDCAN_IT_RX_FIFO0_NEW_MESSAGE|FDCAN_IT_BUS_OFF, 0) != HAL_OK)
    {
        Error_Handler();
    }
    if(HAL_FDCAN_Start(&hfdcan1) != HAL_OK)
    {
        Error_Handler();
    }
}

void HAL_FDCAN_RxFifo0Callback(FDCAN_HandleTypeDef *hfdcan, uint32_t RxFifo0ITs)
{
    uint8_t can_RxMsg[RxQueueItemLen];
    memset(can_RxMsg, 0x0, 13);
    
    if(hfdcan->Instance == FDCAN1)
    {
        if(HAL_FDCAN_GetRxMessage(&hfdcan1, FDCAN_RX_FIFO0, &RxHeader, &can_RxMsg[5]) != HAL_OK)
        {
            Error_Handler();
        }
        
        can_RxMsg[0] = (uint8_t)(RxHeader.Identifier >> 24);
        can_RxMsg[1] = (uint8_t)(RxHeader.Identifier >> 16);
        can_RxMsg[2] = (uint8_t)(RxHeader.Identifier >> 8);
        can_RxMsg[3] = (uint8_t)(RxHeader.Identifier);
        can_RxMsg[4] = (uint8_t)(RxHeader.DataLength >> 16);
        RE_EnQueue(p_FdCanRxQueue, can_RxMsg, RxQueueItemLen);
        packetCnt++;
    }
}

#if 0
void RE_CAN_Format_TxMsg(uint8_t dest_node_id, uint8_t msg_id, uint8_t *p_data)
{
    uint8_t msg_item[TxQueueItemLen] = {0};
    msg_item[0] = TX_NODE_CoController >> 8;                      /* CAN_ID[3] */
    msg_item[1] = (uint8_t)(TX_NODE_CoController | dest_node_id); /* CAN_ID[2] */
    msg_item[2] = 00;
    msg_item[3] = msg_id;
    msg_item[4] = p_data[0]; /* DLC */
    for (uint8_t i = 0; i < msg_item[4]; i++)
    {
        msg_item[5 + i] = p_data[1 + i];
    } 
    RE_EnQueue(p_FdCanTxQueue, msg_item, TxQueueItemLen);
}
#endif

void RE_FDCAN2_Write_Msg(uint8_t *p_CanTxMsg)
{   
    uint32_t DLC = 0;
    DLC = DLC | (p_CanTxMsg[4] << 16);
    TxHeader.Identifier          = (uint32_t)(p_CanTxMsg[0] << 24 | p_CanTxMsg[1] << 16 | p_CanTxMsg[2] << 8 | p_CanTxMsg[3]);;
    TxHeader.IdType              = FDCAN_EXTENDED_ID;
    TxHeader.TxFrameType         = FDCAN_DATA_FRAME;
    TxHeader.DataLength          = DLC;
    TxHeader.ErrorStateIndicator = FDCAN_ESI_PASSIVE;
    TxHeader.BitRateSwitch       = FDCAN_BRS_OFF;
    TxHeader.FDFormat            = FDCAN_CLASSIC_CAN;
    TxHeader.TxEventFifoControl  = FDCAN_NO_TX_EVENTS;
    TxHeader.MessageMarker       = 0;
    /* FIXME: Use interrupt instead of polling for the mailbox empty check.*/
    while(HAL_FDCAN_GetTxFifoFreeLevel(&hfdcan1) == 0);
    if(HAL_FDCAN_AddMessageToTxFifoQ(&hfdcan1, &TxHeader, &p_CanTxMsg[5]) != HAL_OK)
    {
        Error_Handler();
    }
}

#if 0
void RE_FDCAN2_Read_Msg(uint8_t *p_CanRxMsg)
{
    uint8_t io_task[5] = {0};
    if(p_CanRxMsg[1] == 0xF1)
    {
        switch(p_CanRxMsg[3])
        {
            case 0x01:
                io_task[0] = 0x02;
                io_task[1] = p_CanRxMsg[5];
                io_task[2] = p_CanRxMsg[6];
                RE_EnQueue(p_Task_Queue, io_task, TASK_QUEUE_ITEM_LEN);
                break;
            case 0x02:
                io_task[0] = 0x03;
                io_task[1] = p_CanRxMsg[5];
                io_task[2] = p_CanRxMsg[6];
                RE_EnQueue(p_Task_Queue, io_task, TASK_QUEUE_ITEM_LEN);
                break;
            case 0x03:
                io_task[0] = 0x05;
                io_task[1] = p_CanRxMsg[5];
                RE_EnQueue(p_Task_Queue, io_task, TASK_QUEUE_ITEM_LEN);
                break;                
        }   
    }
}
#endif