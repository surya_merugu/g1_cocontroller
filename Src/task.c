#include "task.h"

Queue_t *p_Event_Queue;
Queue_t *p_Task_Queue;
extern Queue_t *p_FdCanRxQueue;
extern Queue_t *p_FdCanTxQueue;
extern Queue_t *p_UartRxQueue, *p_UartTxQueue;
extern UART_HandleTypeDef huart4;
extern uint8_t uartChar;

uint8_t * p_UartRxMsg;
extern bool UartRxDmaError_Flag;
static uint8_t UartDMAStartErrorCnt = 0;
uint32_t t_ms = 0;

#if 0
static void RE_ProcessEvents(uint8_t *p_event);
static void RE_ProcessTasks(uint8_t *p_task);
#endif

void RE_Create_EVT_Queue(void)
{
    p_Event_Queue = RE_CreateQueue(EVT_QUEUE_LEN, EVT_QUEUE_ITEM_LEN);
}

void RE_Create_TASK_Queue(void)
{
    p_Task_Queue = RE_CreateQueue(TASK_QUEUE_LEN, TASK_QUEUE_ITEM_LEN);
}

bool RE_Task_Pending(void)
{
    bool ret_val = false;
    if(RE_IsQueueEmpty(p_UartRxQueue) != 0)
    {
        ret_val |= true;
        p_UartRxMsg = RE_DeQueue(p_UartRxQueue, UartRxQueueItemsLen);
        if(p_UartRxMsg != 0)
        {
            RE_RPI_ReadMsg(p_UartRxMsg);
        }     
    } 
    if(RE_IsQueueEmpty(p_FdCanRxQueue) != 0)
    {
        ret_val |= true;
        uint8_t *p_CanRxMsg = RE_DeQueue(p_FdCanRxQueue, RxQueueItemLen);
        if(p_CanRxMsg != 0)
        {
            RE_EnQueue(p_UartTxQueue, p_CanRxMsg, UartTxQueueItemsLen);
        }   
    }  
    if(RE_IsQueueEmpty(p_FdCanTxQueue) != 0)
    {
        ret_val |= true;
        uint8_t *p_CanTxMsg = RE_DeQueue(p_FdCanTxQueue, TxQueueItemLen);
        if(p_CanTxMsg != 0)
        {
            RE_FDCAN2_Write_Msg(p_CanTxMsg);
        }     
    }  
    if(RE_IsQueueEmpty(p_UartTxQueue) != 0)
    {
        ret_val |= true;
        uint8_t *p_UartTxMsg = RE_DeQueue(p_UartTxQueue, UartTxQueueItemsLen);
        if(p_UartTxMsg != 0)
        {
            RE_RPI_WriteMsg(p_UartTxMsg);
            
        }     
    } 
    if(UartRxDmaError_Flag)
    {
        t_ms = HAL_GetTick();
        while((HAL_GetTick() - t_ms) < 10)
        {
            __NOP();
        }
        if(HAL_UART_Receive_DMA(&huart4, &uartChar, 1) != HAL_OK)
        {
            if(UartDMAStartErrorCnt > 10)
            {
                HAL_NVIC_SystemReset();
            }
            UartDMAStartErrorCnt++;
        } 
        else 
        {
           UartDMAStartErrorCnt = 0;  
           UartRxDmaError_Flag = false;
            __NOP();
        }
    }
    return ret_val;
}

#if 0
void RE_load_system_status(void)
{
}

static void RE_ProcessTasks(uint8_t *p_task)
{
    uint8_t msg_id = p_task[0];
    uint8_t buffer[2] = {0};
    switch (msg_id)
    {
        default:
            break;
    }
}

static void RE_ProcessEvents(uint8_t *p_event)
{
    uint8_t msg_id = p_event[0];
    switch(msg_id)
    {
        default:
            break;
    }
}
#endif