
#include "uart_ring_buffer.h"

extern UART_HandleTypeDef huart4;
extern Queue_t *p_UartRxQueue;
extern Queue_t *p_UartTxQueue;
uint8_t ble_rcvd_char_count = 0;

ring_buffer_uart4 rx_buffer_uart4 = {{0}, 0, 0};
ring_buffer_uart4 tx_buffer_uart4 = {{0}, 0, 0};
ring_buffer_uart4 *_rx_buffer_uart4;
ring_buffer_uart4 *_tx_buffer_uart4;

void Store_UART4_Char(unsigned char c, ring_buffer_uart4 *buffer);

/**
  * @Brief Ringbuf_Init
  * This function initialises the ring buffer and also enables interrupts
  * @Param None
  * @Retval None
  */
void Ringbuf_Init(void)
{
    _rx_buffer_uart4 = &rx_buffer_uart4;
    _tx_buffer_uart4 = &tx_buffer_uart4;

    /* Enable the UART Error Interrupt: (Frame error, noise error, overrun error) */
    __HAL_UART_ENABLE_IT(&huart4, UART_IT_ERR);

    /* Enable the UART Data Register not empty Interrupt */
    __HAL_UART_ENABLE_IT(&huart4, UART_IT_RXNE);
}

/**
  * @Brief Ringbuf_UART1_Free
  * This function frees the ring buffer of UART2
  * @Param None
  * @Retval None
  */
void Ringbuf_UART4_Free(void)
{
    memset(_rx_buffer_uart4->buffer_uart4, 0x00, UART4_BUFFER_SIZE);
    _rx_buffer_uart4->head_uart4 = NULL;
    _rx_buffer_uart4->tail_uart4 = NULL;
    memset(_tx_buffer_uart4->buffer_uart4, 0x00, UART4_BUFFER_SIZE);
    _tx_buffer_uart4->head_uart4 = NULL;
    _tx_buffer_uart4->tail_uart4 = NULL;
}

/**
  * @Brief Store_UART1_Char
  * This function stores the received charcter from UART2 into UART2 ring buffer
  * @Param char 
  * @Param pointer to ting buffer
  * @Retval None
  */
void Store_UART4_Char(unsigned char c, ring_buffer_uart4 *buffer)
{
    uint32_t i = (uint32_t)(buffer->head_uart4 + 1) % UART4_BUFFER_SIZE;
    /** 
   * if we should be storing the received character into the location
   * just before the tail (meaning that the head would advance to the
   * current location of the tail), we're about to overflow the buffer
   * and so we don't write the character or advance the head.
   */
    if (i != buffer->tail_uart4)
    {
        buffer->buffer_uart4[buffer->head_uart4] = c;
        buffer->head_uart4 = i;
    }
}


#if 0
static int UART2_Read(void)
{
    // if the head isn't ahead of the tail, we don't have any characters
    if (_rx_buffer_uart2->head_uart2 == _rx_buffer_uart2->tail_uart2)
    {
        return -1;
    }
    else
    {
        unsigned char c = _rx_buffer_uart2->buffer_uart2[_rx_buffer_uart2->tail_uart2];
        _rx_buffer_uart2->tail_uart2 = (unsigned int)(_rx_buffer_uart2->tail_uart2 + 1) % UART_BUFFER_SIZE;
        return c;
    }
}


static int UART3_Read(void)
{
    // if the head isn't ahead of the tail, we don't have any characters
    if (_rx_buffer_uart3->head_uart3 == _rx_buffer_uart3->tail_uart3)
    {   
        return -1;
    }
    else
    {
        unsigned char c = _rx_buffer_uart3->buffer_uart3[_rx_buffer_uart3->tail_uart3];
        _rx_buffer_uart3->tail_uart3 = (unsigned int)(_rx_buffer_uart3->tail_uart3 + 1) % UART_BUFFER_SIZE;
        return c;
    }
}


//make this better
void Get_UART2_String(char *buffer)
{
    int index = 0;
    unsigned int start = _rx_buffer_uart2->tail_uart2;
    unsigned int end = (_rx_buffer_uart2->head_uart2);
    if ((_rx_buffer_uart2->tail_uart2 > _rx_buffer_uart2->head_uart2) && (_rx_buffer_uart2->buffer_uart2[end - 1] == '\n'))
    {
        for (unsigned int i = start; i < UART_BUFFER_SIZE; i++)
        {
            buffer[index] = UART2_Read();
            index++;
        }
        for (unsigned int i = 0; i < end; i++)
        {
            buffer[index] = UART2_Read();
            index++;
        }
    }
    else if ((_rx_buffer_uart2->buffer_uart2[end - 1] == '\n'))
    {
        for (unsigned int i = start; i < end; i++)
        {
                buffer[index] = UART2_Read();
                index++;
        }
    }
}


void Get_UART3_String(char *buffer)
{
    int index = 0;
    unsigned int start = _rx_buffer_uart3->tail_uart3;
    unsigned int end = (_rx_buffer_uart3->head_uart3);
    if ((_rx_buffer_uart3->tail_uart3 > _rx_buffer_uart3->head_uart3) && (_rx_buffer_uart3->buffer_uart3[end - 1] == '\n'))
    {
        for (unsigned int i = start; i < UART_BUFFER_SIZE; i++)
        {
            buffer[index] = UART3_Read();
            index++;
        }
        for (unsigned int i = 0; i < end; i++)
        {
                buffer[index] = UART3_Read();
                index++;
        }
    }
    else if ((_rx_buffer_uart3->buffer_uart3[end - 1] == '\n'))
    {
        for (unsigned int i = start; i < end; i++)
        {
                buffer[index] = UART3_Read();
                index++;
        }
    }
}
#endif

/**
  * @Brief This function handles UART global interrupt.
  */
void UART_ISR(UART_HandleTypeDef *huart)
{
    if(huart -> Instance == UART4)
    {
        uint32_t isrflags   = READ_REG(huart->Instance->ISR);
        uint32_t cr1its     = READ_REG(huart->Instance->CR1);
        /* if DR is not empty and the Rx Int is enabled */
        if (((isrflags & USART_ISR_RXNE_RXFNE) != RESET) && ((cr1its & USART_CR1_RXNEIE_RXFNEIE) != RESET))
        {
            /******************
            *  @note   PE (Parity error), FE (Framing error), NE (Noise error), ORE (Overrun
            *          error) and IDLE (Idle line detected) flags are cleared by software
            *          sequence: a read operation to USART_SR register followed by a read
            *          operation to USART_DR register.
            * @note   RXNE flag can be also cleared by a read to the USART_DR register.
            * @note   TC flag can be also cleared by software sequence: a read operation to
            *          USART_SR register followed by a write operation to USART_DR register.
            * @note   TXE flag is cleared only by a write to the USART_DR register.

            *********************/
            huart->Instance->ISR;                       /* Read status register */
            unsigned char c = huart->Instance->RDR;     /* Read data register */
            Store_UART4_Char (c, _rx_buffer_uart4);  // store data in buffer
            if('@' == c)
            {
                RE_EnQueue(p_UartRxQueue, rx_buffer_uart4.buffer_uart4, UartRxQueueItemsLen);
                Ringbuf_UART4_Free();            
            }
            return;
        }
        /*If interrupt is caused due to Transmit Data Register Empty */
        if (((isrflags & USART_ISR_TXE_TXFNF) != RESET) && ((cr1its & USART_CR1_TXEIE_TXFNFIE) != RESET))
        {
            if(tx_buffer_uart4.head_uart4 == tx_buffer_uart4.tail_uart4)
            {
                // Buffer empty, so disable interrupts
                __HAL_UART_DISABLE_IT(huart, UART_IT_TXE);
            }
            else
            {
                // There is more data in the output buffer. Send the next byte
                unsigned char c = tx_buffer_uart4.buffer_uart4[tx_buffer_uart4.tail_uart4];
                tx_buffer_uart4.tail_uart4 = (tx_buffer_uart4.tail_uart4 + 1) % UART4_BUFFER_SIZE;
                /******************
                *  @note   PE (Parity error), FE (Framing error), NE (Noise error), ORE (Overrun
                *          error) and IDLE (Idle line detected) flags are cleared by software
                *          sequence: a read operation to USART_SR register followed by a read
                *          operation to USART_DR register.
                * @note   RXNE flag can be also cleared by a read to the USART_DR register.
                * @note   TC flag can be also cleared by software sequence: a read operation to
                *          USART_SR register followed by a write operation to USART_DR register.
                * @note   TXE flag is cleared only by a write to the USART_DR register.

                *********************/
                huart->Instance->ISR;
                huart->Instance->TDR = c;
            }
            return;
        }
    }
}
/************************ (C) COPYRIGHT RACEnergy **********END OF FILE********/