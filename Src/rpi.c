#include "rpi.h"
#include "stdlib.h"
#include "string.h"


UART_HandleTypeDef huart4;
Queue_t *p_UartRxQueue, *p_UartTxQueue;
uint8_t Uart_Rx_Buffer[17];
uint8_t uartPacketCnt;
uint8_t uartRxPcktCnt, uartChar ;
extern Queue_t *p_FdCanTxQueue;
volatile bool UartRxDmaError_Flag = false;
/**
  * @brief UART4 Initialization Function
  * @param None
  * @retval None
  */
void RE_UART4_Init(void)
{

  /* USER CODE BEGIN UART4_Init 0 */

  /* USER CODE END UART4_Init 0 */

  /* USER CODE BEGIN UART4_Init 1 */

  /* USER CODE END UART4_Init 1 */
  huart4.Instance = UART4;
  huart4.Init.BaudRate = 115200;
  huart4.Init.WordLength = UART_WORDLENGTH_8B;
  huart4.Init.StopBits = UART_STOPBITS_1;
  huart4.Init.Parity = UART_PARITY_NONE;
  huart4.Init.Mode = UART_MODE_TX_RX;
  huart4.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart4.Init.OverSampling = UART_OVERSAMPLING_16;
  huart4.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
  huart4.Init.ClockPrescaler = UART_PRESCALER_DIV1;
  huart4.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
  if (HAL_UART_Init(&huart4) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_UARTEx_SetTxFifoThreshold(&huart4, UART_TXFIFO_THRESHOLD_1_8) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_UARTEx_SetRxFifoThreshold(&huart4, UART_RXFIFO_THRESHOLD_1_8) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_UARTEx_DisableFifoMode(&huart4) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN UART4_Init 2 */
    p_UartTxQueue = RE_CreateQueue(UartTxQueueItems, UartTxQueueItemsLen);
    p_UartRxQueue = RE_CreateQueue(UartRxQueueItems, UartRxQueueItemsLen);
//    Ringbuf_Init();
    HAL_UART_Receive_DMA(&huart4, &uartChar, 1);
  /* USER CODE END UART4_Init 2 */
}
#if 0
void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{   
//    printf("%s\n", Uart_Rx_Buffer);
    if(Uart_Rx_Buffer[0] == '#' && Uart_Rx_Buffer[16] == '@')
    {
        RE_EnQueue(p_UartRxQueue, Uart_Rx_Buffer, UartRxQueueItemsLen);
    }
    __NOP();
    memset(Uart_Rx_Buffer, 0x0, 17);
    memset(Uart_Rx_Buffer, 0x0, 17);
    uartRxPcktCnt++;
    HAL_UART_Receive_DMA(&huart4, Uart_Rx_Buffer, 17);
}
#endif

void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{   
  static uint8_t uartBufHead =0;
  uartBufHead = (uartBufHead >= UartRxQueueItemsLen) ? 0:uartBufHead;   /* Reset the index if it overflows (AOOB -Array out of bound )*/
  
    if(uartChar == '#'){
        uartBufHead =0;
        memset(Uart_Rx_Buffer,0,UartRxQueueItemsLen);
        Uart_Rx_Buffer[uartBufHead++] = uartChar;
    }
    else if(uartChar == '@' && uartBufHead == 16 && Uart_Rx_Buffer[0] == '#'){
        Uart_Rx_Buffer[uartBufHead] = uartChar;
        RE_EnQueue(p_UartRxQueue, Uart_Rx_Buffer, UartRxQueueItemsLen);
        uartBufHead = 0;
    }
    else if(uartChar == '@'){
        memset(Uart_Rx_Buffer,0,UartRxQueueItemsLen);
        uartBufHead = 0;
    }
    else{
         Uart_Rx_Buffer[uartBufHead++] = uartChar;
    }
	
    __NOP();
//    uartRxPcktCnt++;
    if(HAL_UART_Receive_DMA(&huart4, &uartChar, 1) != HAL_OK)
    {
        UartRxDmaError_Flag = true;
    }
}

void RE_RPI_ReadMsg(uint8_t *p_Ec20RxMsg)
{
    uint8_t QueueItem[13];
    for (uint8_t i = 6, j = 1; i >= 0 && j < 8; i--, j++)
    {
        if ((p_Ec20RxMsg[14] >> i) & 0x01)
        {
            p_Ec20RxMsg[j] = p_Ec20RxMsg[j] - 1;
        }
    }
    for (uint8_t i = 6, j = 8; i >= 1 && j < 14; i--, j++)
    {
        if ((p_Ec20RxMsg[15] >> i) & 0x01)
        {
            p_Ec20RxMsg[j] = p_Ec20RxMsg[j] - 1;
        }
    }
    memcpy(QueueItem, &p_Ec20RxMsg[1], 13);
    __NOP();
    __NOP();
    RE_EnQueue(p_FdCanTxQueue, QueueItem, TxQueueItemLen);
}

void RE_RPI_WriteMsg(uint8_t *p_Ec20TxMsg)
{
    uint8_t UartTx_Buffer[17];
    memset(UartTx_Buffer, 0x00, 17);
    UartTx_Buffer[0] = 0x23;
    UartTx_Buffer[16] = 0x40;
    UartTx_Buffer[14] |= (1 << 7);
    UartTx_Buffer[15] |= (1 << 7);
    for (uint8_t i = 0; i < 13; i++)
    {
        if ((p_Ec20TxMsg[i] == 0x23) || (p_Ec20TxMsg[i] == 0x40))
        {
            p_Ec20TxMsg[i] = p_Ec20TxMsg[i] + 1;
            if (i < 7)
            {
                UartTx_Buffer[14] |= (1 << (6 - i));
            }
            else
            {
                UartTx_Buffer[15] |= (1 << (13 - i));
            }
        }
    }
    memcpy(&UartTx_Buffer[1], p_Ec20TxMsg, 13);
    __NOP();
    
//    printf("%d\n", UartTx_Buffer[1]);
    if (HAL_UART_Transmit(&huart4, (uint8_t *)UartTx_Buffer, 17, 10) != HAL_OK)
    {
        Error_Handler();
    }
    uartPacketCnt++;
}